# Screw Hole Samples

## Description

CAD files (.FCstd, .STL and .cbddlp) for 3d printing a plate with sample of holes ranging from 1.0mm-5.0mm

## Photo

![](photo.png)
